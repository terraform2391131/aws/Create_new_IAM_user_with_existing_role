data "aws_caller_identity" "current" {}

resource "aws_iam_user" "new_user" {
  name = join("_", [var.user_name, var.policy_name])
  path = join("", ["/", var.user_department]) # group user by path
}

resource "aws_iam_access_key" "new_access_key" {
  user = aws_iam_user.new_user.name
}

# adding standard policy do user
resource "aws_iam_user_policy_attachment" "add_existing_policy" {
  user       = aws_iam_user.new_user.name
  policy_arn = join("", ["arn:aws:iam::aws:policy/", var.policy_name])
}

resource "aws_iam_user_policy_attachment" "ChangePassword_policy" {
  user       = aws_iam_user.new_user.name
  policy_arn = "arn:aws:iam::aws:policy/IAMUserChangePassword"
}

# create access to AWS Management Console
resource "aws_iam_user_login_profile" "new_user_profile" {
  user = aws_iam_user.new_user.name
  password_reset_required = true
  password_length = 16
}












