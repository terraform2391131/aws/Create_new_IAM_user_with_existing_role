# Create_new_IAM_user_with_existing_policy
#What's included:
#Create new user in AWS
#Add existing role to a new created user
#Create web_services / menagment console access for new user
#Create CLI/SDK access for new user

user_name = "Daniel"
policy_name = "AmazonEC2ReadOnlyAccess" # https://us-east-1.console.aws.amazon.com/iamv2/home#/policies
user_department = ""   # optional

#How to use:
#terraform init
#terraform apply --auto-approve
#terraform output -json