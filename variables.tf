variable "user_name"{
    description = "Please enter new user name"
    type = string
    default = "JohnDoe"
}

variable "policy_name" {
    description = "Existing policy"
    type = string
    default = "project1"
}

variable "user_department" {
    description = "Group users on AWS by department"
    type = string
    default = ""
}
